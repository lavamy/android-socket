package com.uphave.messenger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MessageAdapter extends ArrayAdapter<Message> {
    private final Context context;
    private final List<Message> values;

    public MessageAdapter(Context context, List<Message> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.item_message, parent, false);

        TextView name = (TextView) rootView.findViewById(R.id.name);
        TextView time = (TextView) rootView.findViewById(R.id.time);
        TextView message = (TextView) rootView.findViewById(R.id.message);

        Message msg = values.get(position);

        name.setText(msg.getUsername());
        time.setText(SimpleDateFormat.getDateTimeInstance().format(new Date(msg.getTimestamp())));
        message.setText(msg.getMessage());

        return rootView;
    }
}