package com.uphave.messenger;

public interface SocketServiceCallback {
    void onMessageReceived(Message msg);
}
