package com.uphave.messenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    public static final String PREF_NAME = "PREFS";
    public static final String KEY_USERNAME = "username";
    private EditText mUsernameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String username = getSharedPreferences(PREF_NAME, MODE_PRIVATE).getString(KEY_USERNAME, null);
        if (!TextUtils.isEmpty(username)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(KEY_USERNAME, username);
            startActivity(intent);
            return;
        }

        setContentView(R.layout.activity_login);
        mUsernameView = (EditText) findViewById(R.id.username);
    }

    public void signUp(View view) {
        mUsernameView.setError(null);

        String username = mUsernameView.getText().toString();

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            mUsernameView.requestFocus();
        }
        else if (username.length() < 6) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            mUsernameView.requestFocus();
        }
        else {
            getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit().putString(KEY_USERNAME, username).commit();
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(KEY_USERNAME, username);
            startActivity(intent);
        }
    }
}

