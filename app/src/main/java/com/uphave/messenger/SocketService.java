package com.uphave.messenger;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class SocketService extends Service {

    private static final String TAG = SocketService.class.getSimpleName();
    private final IBinder mBinder = new SocketBinder();

    private SocketServiceCallback mCallback;

    private Socket mClientSocket;
    private ServerSocket mServerSocket;

    private String mUsername;

    private boolean mWorkerEnabled;

    public SocketService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        login();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logout();
    }

    public void logout() {
        mWorkerEnabled = false;

        try {
            if(mClientSocket != null)
                mClientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if(mServerSocket != null)
                mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mCallback = null;
        return super.onUnbind(intent);
    }

    public void login() {
        mWorkerEnabled = true;
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new ServerWorker());
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new ClientWorker());
    }

    public Message send(String message) {
        try {
            Message msg = new Message("dummy", mUsername, message, new Date().getTime());
            PrintWriter output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(mClientSocket.getOutputStream())), true);

            output.println(msg.getUserId());
            output.println(msg.getUsername());
            output.println(msg.getMessage());
            output.println(msg.getTimestamp());

            return msg;
        } catch (Exception e) {
            return null;
        }
    }

    public class SocketBinder extends Binder {
        SocketService getService(String username, SocketServiceCallback callback) {
            mCallback = callback;
            mUsername = username;
            return SocketService.this;
        }
    }

    class ClientWorker implements Runnable {

        @Override
        public void run() {
            while (mWorkerEnabled) {
                try {
                    InetAddress serverAddress = InetAddress.getByName(getString(R.string.server_ip));
                    mClientSocket = new Socket(serverAddress, getResources().getInteger(R.integer.client_port));
                    Log.i(TAG, "Client connected");
                    break;
                } catch (Exception e1) {
                }
            }
        }
    }

    class ServerWorker implements Runnable {

        public void run() {
            while(true) {
                try {
                    mServerSocket = new ServerSocket();
                    mServerSocket.setReuseAddress(true);
                    mServerSocket.bind(new InetSocketAddress(getResources().getInteger(R.integer.server_port)));
                    Log.i(TAG, "Server created");
                    break;
                } catch (IOException e) {
                }
            }

            while (mWorkerEnabled) {
                try {
                    AsyncTask.THREAD_POOL_EXECUTOR.execute(new CommunicationWorker(mServerSocket.accept()));
                    Log.i(TAG, "New connection");
                } catch (IOException e) {
                }
            }
        }
    }

    class CommunicationWorker implements Runnable {

        private Socket mCommunicationSocket;

        public CommunicationWorker(Socket communicationSocket) {
            this.mCommunicationSocket = communicationSocket;
        }

        public void run() {
            while (mWorkerEnabled) {
                try {
                    BufferedReader input = new BufferedReader(new InputStreamReader(mCommunicationSocket.getInputStream()));

                    String id = input.readLine();
                    String username = input.readLine();
                    String message = input.readLine();
                    long time = Long.parseLong(input.readLine());

                    Message msg = new Message(id, username, message, time);
                    Log.e(TAG, "Received: " + msg);

                    if (mCallback != null) {
                        mCallback.onMessageReceived(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}