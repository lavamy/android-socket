package com.uphave.messenger;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText mEdtMessage;
    private ListView mLstMessages;
    private List<Message> mMessages;
    private MessageAdapter mAdapter;
    private SocketService mService;
    private String mUsername;

    private SocketServiceCallback mCallback = new SocketServiceCallback() {
        @Override
        public void onMessageReceived(final Message msg) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMessages.add(msg);
                    mAdapter.notifyDataSetChanged();
                    mLstMessages.setSelection(mAdapter.getCount() - 1);
                }
            });
        }
    };


    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SocketService.SocketBinder binder = (SocketService.SocketBinder) service;
            mService = binder.getService(mUsername, mCallback);
            mService.login();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLstMessages = (ListView) findViewById(R.id.message_list);
        mEdtMessage = (EditText) findViewById(R.id.message);

        mMessages = new ArrayList<>();
        mAdapter = new MessageAdapter(this, mMessages);
        mLstMessages.setAdapter(mAdapter);

        mUsername = getIntent().getStringExtra(LoginActivity.KEY_USERNAME);

        Intent intent = new Intent(this, SocketService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mConnection);
            mService = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout:
                logout();
                break;
            case R.id.send:
                send();
                break;
        }
    }

    private void logout() {
        if (mService == null) {
            Toast.makeText(this, "Service is not available", Toast.LENGTH_SHORT).show();
            return;
        }
        mService.logout();
        getSharedPreferences(LoginActivity.PREF_NAME, MODE_PRIVATE).edit().remove(LoginActivity.KEY_USERNAME).commit();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void send() {
        String message = mEdtMessage.getText().toString();
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(this, "Message cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mService == null) {
            Toast.makeText(this, "Service is not available", Toast.LENGTH_SHORT).show();
            return;
        }

        Message msg = mService.send(message);
        if (msg != null) {
            mMessages.add(msg);
            mAdapter.notifyDataSetChanged();
            mLstMessages.setSelection(mAdapter.getCount() - 1);
        }
        else {
            Toast.makeText(this, "Cannot send message", Toast.LENGTH_SHORT).show();
        }
    }
}