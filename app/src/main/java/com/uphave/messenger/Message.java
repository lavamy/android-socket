package com.uphave.messenger;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Message implements Parcelable, Serializable {
    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
    private String mUserId;
    private String mUsername;
    private String mMessage;
    private long mTimestamp;

    public Message(String userId, String username, String message, long timestamp) {
        mUserId = userId;
        mUsername = username;
        mMessage = message;
        mTimestamp = timestamp;
    }

    protected Message(Parcel in) {
        mUserId = in.readString();
        mUsername = in.readString();
        mMessage = in.readString();
        mTimestamp = in.readLong();
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(long timestamp) {
        mTimestamp = timestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(mUserId);
        dest.writeString(mUsername);
        dest.writeString(mMessage);
        dest.writeLong(mTimestamp);
    }

    @Override
    public String toString() {
        return String.format("(%s, %s, %s, %ld)", mUserId, mUsername, mMessage, mTimestamp);
    }
}
